<?php

namespace App\Http\Controllers;

use App\age_classes;
use App\equipments;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

use App\meets;

use Illuminate\Support\Facades\DB;



class import extends Controller
{


	private function sanitize($string){

		return str_replace('"', "", $string);

	}


	private function getVeriler(){

		$handle = file_get_contents(public_path('openpowerlifting.csv'), "r");

		$satirlar = explode(PHP_EOL, $handle);


		$veriler = [];



		foreach($satirlar as $satir){

			$veriler[] = explode(',', $satir);

		}

		return $veriler;

	}


	public function index(){




		echo 'basladi';



		$veriler = $this->getVeriler();


		echo "--toplam veri:".count($veriler).'--';






		// age_class bilgisi olmayan competitors'lar için age_class oluştur.
		DB::table('age_classes')->insert([

			'age_min' => -1,
			'age_max' => -1

		]);

		$null_age_class_id = DB::getPdo()->lastInsertId();




		$i = 0;
		foreach($veriler as $veri){

			$i++;

			if($i==1)
				continue;


			echo $i.",";


			/**
			 * Eğer kayıt mevcutsa, mevcut kaydı bul
			 */


			$age_class_string = $this->sanitize($veri[5]);

			$age_class_arr = explode("-", $age_class_string);



			if(strpos($age_class_string, '-') === false){

				// format hatalı

				$age_classes_id = $null_age_class_id;

			}else{

				$age_class_arr = explode("-", $age_class_string);

				$age_min = intval($age_class_arr[0]);
				$age_max = intval($age_class_arr[1]);


				$age_classes = age_classes::where('age_min',$age_min)->where('age_max', $age_max)->first();

				if($age_classes){

					$age_classes_id = $age_classes->id;

				}else{


					DB::table('age_classes')->insert([

						'age_min' => $age_min,
						'age_max' => $age_max

					]);

					$age_classes_id = DB::getPdo()->lastInsertId();

				}

			}
			





			/**
			 * Eğer kayıt mevcutsa, mevcut kaydı bul
			 */

			$mevcut_equipment = equipments::where('name',$this->sanitize($veri[3]))->first();

			if($mevcut_equipment){


				$equipment_id = $mevcut_equipment->id;


			}else{

				DB::table('equipments')->insert([

					'name' => $this->sanitize($veri[3])

				]);

				$equipment_id = DB::getPdo()->lastInsertId();

			}



			DB::table('formulas')->insert([

				'wilks' => floatval($this->sanitize($veri[26])),
				'gloss_brenner' => floatval($this->sanitize($veri[28])),
				'ipf_points' => floatval($this->sanitize($veri[29])),
				'mc_culloch' => floatval($this->sanitize($veri[27])),

			]);

			$formulas_id = DB::getPdo()->lastInsertId();


			/**
			 * Eğer kayıt mevcutsa, mevcut kaydı bul
			 */

			$date = $this->sanitize($veri[33]);

			$date = date('Y-m-d', strtotime($date));

			$mevcut_meet = meets::where('name',$this->sanitize($veri[36]))->where('state', $this->sanitize($veri[35]))->where('country', $this->sanitize($veri[34]))->where('date', $date)->where('federation', $this->sanitize($veri[32]))->first();

			if($mevcut_meet) {


				$meets_id = $mevcut_meet->id;


			}else{


				DB::table('meets')->insert([

					'name' => $this->sanitize($veri[36]),
					'state' => $this->sanitize($veri[35]),
					'country' => $this->sanitize($veri[34]),
					'date' => $date,
					'federation' => $this->sanitize($veri[32]),

				]);

				$meets_id = DB::getPdo()->lastInsertId();


			}





			$total_kg_best_toplamlari = intval($veri[13]) + intval($veri[18]) + intval($veri[23]);


			try{


				DB::table('competitors')->insert([

					'name' => $this->sanitize($veri[0]),
					'sex' => $this->sanitize($veri[1]),
					'event' => $this->sanitize($veri[2]),
					'equipments_id' => $equipment_id,
					'age' => $this->sanitize($veri[4]),
					'age_classes_id' => $age_classes_id,
					'meets_id' => $meets_id,
					'formulas_id' => $formulas_id,
					'tested' => intval($this->sanitize($veri[30])),
					'body_weight' => floatval($this->sanitize($veri[7])),
					'total_kg' => floatval($total_kg_best_toplamlari),
					'division' => $this->sanitize($veri[6]),
					'weight_class_kg' => floatval($this->sanitize($veri[8])),
					'place' => $this->sanitize($veri[25])

				]);

				$competitors_id = DB::getPdo()->lastInsertId();


			}catch(\Exception $e){

				echo "İşlem yapılamadı: id:".$i." hatalı isim:".$this->sanitize($veri[0])."::".$e->getMessage();
				return;

			}



			$skorlar = [


				'squat' => 9,
				'bench' => 14,
				'deadlift' => 19


			];


			/**
			 *
			 * squat 9 başlangıç: kg = 9 - 9 + 1 = 1;
			 * squat 10   kg = 10 - 9 + 1 = 2;
			 */


			foreach($skorlar as $exercise_type => $baslangic_key ){



				// 4 sefer tekrar et.
				for($j=$baslangic_key; $j<=$baslangic_key+3; $j++) {

					$kg = $j - $baslangic_key + 1;


					DB::table('scores')->insert([

						'competitors_id' => $competitors_id,
						'score_value' => intval($this->sanitize($veri[$j])),
						'attempt_kg' => intval($kg),
						'exercises_type' => $exercise_type

					]);


				}




			}








		}









	}


}
